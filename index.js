const mongoose = require("mongoose");
const usersRoute = require("./routes/user.route");
const express = require("express");
const cors = require("cors");
const app = express();

//connect to mongodb
mongoose
.connect("mongodb://localhost/test", { useNewUrlParser: true })
.then(() => console.log("Connected to MongoDB..."))
.catch(err => console.error("Could not connect to MongoDB..."));

app.use(express.json());
app.use(cors()); // todo: fix this to handle only our domain, see user route.js commented out code
app.use("/auth/users", usersRoute);

const port = process.env.PORT || 4444; // todo: make procces.env.PORT work (currently 4444 is taken)
app.listen(port, () => console.log(`Listening on port ${port}...`));
