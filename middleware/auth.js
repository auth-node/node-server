const jwt = require("jsonwebtoken");
const fs = require('fs');

module.exports = function(req, res, next) {
  const publicKEY  = fs.readFileSync('./keys/public.key', 'utf8');
  //get the token from the header if present
  const token = req.headers.authorization;
  //if no token found, return response (without going to the next middelware)
  if (!token) {
    return res.status(401).send("Access denied. No token provided."); 
  }

  try {
    const verifyOptions = {
      expiresIn:  "12h",
      algorithm:  ["RS256"]
    };
    //if can verify the token, set req.user and pass to next middleware
    console.log(token);
    req.user = jwt.verify(token, 'ABCD123 ');
    next();
  } catch (ex) {
    //if invalid token
    console.log(ex);
    res.status(400).send("Invalid token.");
  }
};
