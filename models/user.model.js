const jwt = require('jsonwebtoken');
const Joi = require('joi');
const mongoose = require('mongoose');
const fs   = require('fs');
const publicKey  = fs.readFileSync('./keys/public.key', 'utf8');

const UserSchema = new mongoose.Schema({
  username: {
    type: String,
    required: true,
    minlength: 5,
    maxlength: 255,
    unique: true
  },
  password: {
    type: String,
    required: true,
    minlength: 3,
    maxlength: 255
  }
});



// custom method to generate authToken 
UserSchema.methods.generateAuthToken = function() {
  // const signOptions = {
  //   expiresIn:  "12h",
  //   algorithm:  "RS256"
  // };
  const token = jwt.sign({_id: this._id}, 'ABCD123');
  return token;
};

const User = mongoose.model('User', UserSchema);

// function to validate user 
function validateUser(user) {
  const schema = {
    username: Joi.string().min(5).max(255).required(),
    password: Joi.string().min(3).max(255).required()
  };

  return Joi.validate(user, schema);
}

exports.User = User;
exports.validate = validateUser;
