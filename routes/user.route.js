const auth = require("../middleware/auth");
const bcrypt = require("bcrypt");
const { User, validate } = require("../models/user.model");
const express = require("express");
const router = express.Router();

// const whitelist = ['http://localhost:3000']
// const corsOptions = {
//   origin: function (origin, callback) {
//     console.log(origin);
//     if (whitelist.indexOf(origin) !== -1) {
//       callback(null, true);
//     } else {
//       callback(new Error('Not allowed by CORS'));
//     }
//   }
// };

router.get("/me", auth, async (req, res) => {
  const user = await User.findById(req.user._id).select("-password");
  res.send(user);
});

router.post("/", async (req, res) => {
  // validate the request body first
  const { error } = validate(req.body);
  if (error) {
    return res.status(400).send(error.details[0].message); 
  }

  //find an existing user
  let user = await User.findOne({ username: req.body.username });
  if (user) {
    return res.status(400).send("User already registered."); 
  }

  user = new User({
    username: req.body.username,
    password: req.body.password
  });
  user.password = await bcrypt.hash(user.password, 10);
  await user.save();

  const access = user.generateAuthToken();
  res.send({
    _id: user._id,
    username: user.username,
    access: access
  });
});

module.exports = router;
